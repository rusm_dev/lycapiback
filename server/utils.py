import bcrypt
import psycopg2
import hashlib
from psycopg2 import OperationalError
from cfg import DevConfig as cfg


# connect to PsQl
def create_connection(db_name, db_user, db_password, db_host, db_port):
    connection = None
    try:
        connection = psycopg2.connect(
            database=db_name,
            user=db_user,
            password=db_password,
            host=db_host,
            port=db_port,
        )
        print("Connection to PostgreSQL DB successful")
    except OperationalError as e:
        print(f"The error '{e}' occurred")
    return connection


# hashing password by hashpw-method
def hash_password(password):
    return bcrypt.hashpw(password.encode(), bcrypt.gensalt()).decode()


# unhashing and checked password by hashpw-method
def check_password(password, hash):
    return bcrypt.checkpw(password.encode(), hash.encode())
